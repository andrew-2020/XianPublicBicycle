//logs.js
var util = require('../../utils/util.js')
Page({
  data: {
    display: 'block',
    logs: []
  },
  onLoad: function () {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(function (log) {
        return util.formatTime(new Date(log))
      })
    })
  },
  closeGuidePage(){
    console.log('Loading ……');
    this.setData({
      display: 'none'
    });
  }
})
